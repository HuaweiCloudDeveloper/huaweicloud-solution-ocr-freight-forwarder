[TOC]

**解决方案介绍**
===============
该解决方案基于华为云文字识别 OCR和人证核身服务 IVS AI技术构建，提供了一个开箱即用的网络货运认证解决方案，支持以下六种场景：身份证、行驶证、驾驶证、道路运输证、道路运输从业资格证的审查和人证核身。

解决方案实践详情页面：https://www.huaweicloud.com/solution/implementations/ocr-freight-forwarder.html

**架构图**
---------------
![方案架构](./document/ocr-freight-forwarder.png)

**架构描述**
---------------
该解决方案会部署如下资源：

1、创建两个对象存储服务 OBS桶，一个用于存放用户上传的证件图片和人脸图片，当用户上传后，自动通知函数工作流 FunctionGraph进行处理；并将证件识别结果和人证核身结果以JSON文件格式返回至另一个OBS桶中。

2、创建函数工作流 FunctionGraph，用于实现调用文字识别 OCR服务及人脸识别服务 IVS业务业务逻辑，当收到OBS上传人脸信息通知后，自动从OBS桶内获取识别出的姓名、身份证号、人脸信息并调用人证核身服务 IVS进行人证核身，并将结果转存到OBS桶里。

3、文字识别 OCR和人证核身服务 IVS：提供证件识别和人证核身服务，识别用户上传的证件图片以及人脸图片核对，并将结果以JSON格式返回。


**组织结构**
---------------
``` lua
huaweicloud-solution-ocr-freight-forwarder
├── ocr-freight-forwarder.tf.json -- 资源编排模板
├── functiongraph
	├── ocr_decompression.py -- 解压函数文件
	├── ocr_network_freight_certification.py  -- 证件识别函数文件
```
**开始使用**
---------------

1、登录对象存储服务 OBS控制台，在桶列表选择快速部署 步骤三创建的用于上传证件及人脸图片的OBS桶，可直接上传证件图片或上传压缩包（支持zip及tar格式），若上传文件为压缩包，则会自动解压至当前桶，文件命名格式（身份证图片：以id开头，驾驶证图片：以driver开头，行驶证图片：以vehicie开头，道路运输证：以transport开头，道路运输从业资格证：以qualification开头，人脸图片：以face开头）不按照此规则命名会导致本方案无法使用，如下图所示：

图1 证件桶

![证件桶](./document/readme-image-001.png)

2、在桶列表选择部署指南快速部署 步骤三创建的用于存放识别人证结果的OBS桶，单击进入即可查看识别结果。

图2 结果桶

![结果桶](./document/readme-image-002.png)


